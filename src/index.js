import { resolve } from 'path';
import dotenv from 'dotenv';
import React from 'react';
import ReactPDF from '@react-pdf/renderer';

import { File } from './File';

dotenv.config();

const { OUTPUT_DIR, FILENAME } = process.env;
const outdir = resolve(__dirname, `../${OUTPUT_DIR}`);

ReactPDF.render(<File />, `${outdir}/${FILENAME}.pdf`);